package Api;

import java.util.Iterator;

public interface IHashTable<Key extends Comparable<Key>, Value extends Comparable<Value>> {

	public int hash(Key key);

	public void put(Key key, Value val);

	public Value get(Key key);

	public boolean contains(Key key);

	public boolean isEmpty();

	public int size();

	public Iterator<Key> keys();
}
