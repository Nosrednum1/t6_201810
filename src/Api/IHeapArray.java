package Api;

/**
 * 
 * @author <b>Anderson Barrag�n Agudelo</b>
 *
 * @param <T> Data to storage in the Heap
 */
public interface IHeapArray<T extends Comparable<T>>{

	/**
	 *  insert a new element in the array
	 * @param element the element to insert
	 */
	public void insert(T element);

	/**
	 * toma y elimina el elemento m�s relevante del mont�culo
	 * @return the most significant element of the Heap
	 */
	public T remove();

	/**
	 * @return the number of elements in the Heap
	 */
	public int size();

	/**
	 * compara i con respecto a j
	 * @param i
	 * @param j
	 * @return
	 */
	public boolean less(int i, int j);

	public void exchange(int i, int j);
}
