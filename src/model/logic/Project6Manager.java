package model.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

public class Project6Manager {

	public  final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public  final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public  final String DIRECCION_LARGE_JSON = "large";
	public  final String DIRECCION_LARGE= "./data/Large/taxi-trips-wrvz-psew-subset-0";
	public  final String COMPLEMENT_DIR = "-02-2017.json";
	public  final String DIRECCION_TEST_JSON = "./data/Test.json";
	public  final int CANTIDAD_ARCHIVOS = 7;

	public void load(String ruta){	Gson g = new GsonBuilder().create();
	if(!ruta.equals(DIRECCION_LARGE_JSON)) cargaSML(g, ruta);
	else for(int i = 2; i<CANTIDAD_ARCHIVOS+2;i++) cargaSML(g, DIRECCION_LARGE + i + COMPLEMENT_DIR);}
	
	private void cargaSML(Gson g, String dirJson){
	try{FileInputStream stream = new FileInputStream(new File(dirJson));
		JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8")); reader.beginArray();
		while (reader.hasNext()) {}		reader.close();
	}catch(Exception e){System.out.println("Error en la carga del los archivos");e.printStackTrace();}}
}
