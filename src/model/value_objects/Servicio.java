package model.value_objects;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>{	

	private String trip_id, taxi_id, company;
	//	______________________
	private String trip_start_timestamp, trip_end_timestamp;
	private int dropoff_community_area, pickup_community_area, trip_seconds;
	private double trip_miles, trip_total;

	public void setId(String id){this.trip_id = id;}

	public int getPickArea(){return pickup_community_area;}
	public int getDropArea(){return dropoff_community_area;}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId(){return trip_id;}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {return taxi_id;}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {return trip_seconds;}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {return trip_miles;}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {return trip_total;}

	public String getTripStart() {return trip_start_timestamp;}

	public String getTripEnd() {return trip_end_timestamp;}

	public String getCompany(){return company;}

	public boolean hasCompany() {return (this.company != null && this.company != "");}

	public boolean estaEnRango(RangoFechaHora rango){
		boolean estaIn = false, estaOut = false;
		String fechaIn = rango.getFechaInicial();	String fechaFn = rango.getFechaFinal();
		if(trip_start_timestamp != null && trip_start_timestamp.compareTo(fechaIn) >= 0)estaIn = true;
		if(trip_end_timestamp != null && (trip_end_timestamp.compareTo(fechaFn)<=0))estaOut = true;
		return (estaIn && estaOut);}

	@Override
	public int compareTo(Servicio arg0) {
		return (this.trip_id.compareTo(arg0.getTripId())<0)?-1:
			(this.trip_id.compareTo(arg0.getTripId())>0)?1:0;}

	public boolean equals(Servicio srvc) {
		if(this == srvc)return true;
		if(srvc == null)return false;
		if(srvc.getClass() != this.getClass())return false;

		if(srvc.getTripId() != this.trip_id)return false;
		return true;
	}

	@Override
	public int hashCode() {
		return trip_id.hashCode();
	}
}
