package model.value_objects;

/**
 * Modela una rango de fechas y horas (iniciales y finales)
 *
 */
public class RangoFechaHora{
	//ATRIBUTOS
	/**
	 * Modela la fecha inicial del rango
	 */
	private String fechaInicial; 

	/**
	 * Modela la fecha final del rango
	 */
	private String fechaFinal;

	//CONSTRUCTOR
	/**
	 * @param pFechaInicial, fecha inicial del rango
	 * @param pFechaFinal, fecha final del rango
	 */
	public RangoFechaHora(String pFechaInicial, String pFechaFinal, String pHoraInic, String pHoraFin){
		this.fechaFinal = pFechaFinal + "T" +pHoraFin;
		this.fechaInicial = pFechaInicial + "T" +pHoraInic;
	}
	//M�TODOS

	/**
	 * @return the fechaInicial
	 */
	public String getFechaInicial() {return fechaInicial;}

	/**
	 * @return the fechaFinal
	 */
	public String getFechaFinal() {return fechaFinal;}
}
