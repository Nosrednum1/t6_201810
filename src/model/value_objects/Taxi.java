package model.value_objects;

import model.data_structures.DoubleLinkedList;

public class Taxi implements Comparable<Taxi>{

	private String company;
	private String taxi_id;
	private double trips_total;
	private double trips_miles_total;
	private DoubleLinkedList<Servicio> servicios;

	public Taxi(String id, String comp) {
		servicios = new DoubleLinkedList<>();
		this.taxi_id = id;
		this.company = (comp!= null)?comp:"Independent Owner";
	}

	public String getTaxiId() {
		return this.taxi_id;
	}

	public String getCompany() {
		return company;
	}
	public double getCash() {
		return trips_total;
	}
	public double getMiles() {
		return trips_miles_total;
	}
	public void addService(Servicio s) {
		this.servicios.add(s);
	}

	public int getCantOfServices() {
		return servicios.size();
	}

	@Override
	public int compareTo(Taxi arg0) {
		return taxi_id.compareTo(arg0.getTaxiId());
	}

	public boolean equals(Taxi taxi) {
		if(this == taxi)return true;
		if(taxi == null)return false;
		if(taxi.getClass() != this.getClass())return false;

		if(taxi.getTaxiId() != this.taxi_id)return false;
		return true;
	}

	@Override
	public int hashCode() {
		return taxi_id.hashCode();
	}
}
