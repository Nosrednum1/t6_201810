package model.data_structures;

import java.util.Iterator;

import Api.IHashTable;

public class SeparateChainingHashTable<Key extends Comparable<Key>, Value extends Comparable<Value>>
implements IHashTable<Key, Value>{

	private int size;
	private Node[] llaves;

	public SeparateChainingHashTable(int capacity) {
		llaves = new Node[capacity];
	}

	private static class Node{
		private Node next;	private Comparable key, val;
		public Node(Comparable key, Comparable val, Node next)  {
			this.next = next; this.key = key; this.val = val;	}
		public Node next() {return next;}}

	@Override
	public int hash(Key key) {
		return (key.hashCode() & 0x7fffffff) % llaves.length;
	}private int hash(Key key, int module) {return (key.hashCode() & 0x7fffffff) % module;} 

	private void expand() {
		Node[] newLlaves = new Node[(llaves.length)*2];
		for (int i = 0; i < size; i++) {
			for(Node x = llaves[i]; x != null; x = x.next) {
				int j = hash((Key) llaves[i].key, newLlaves.length);
				//TODO remade the method expand to replace correctly all elements 
				//				if(newLlaves[i].key.compareTo((Key) x.key) == 0) { x.val = x.val; continue;}
				newLlaves[i] = new Node(llaves[i].key, llaves[i].val, newLlaves[i]);
			}
		}
		llaves = newLlaves;
	}

	@Override
	public void put(Key key, Value val) {
		int i = hash(key);
		for (Node x = llaves[i];x != null; x = x.next()) 
			if(key.compareTo((Key) x.key) == 0) { x.val = val; return;}
		llaves[i] = new Node(key, val, llaves[i]);
	}

	@Override
	public Value get(Key key) {
		int i = hash(key);
		for (Node x = llaves[i]; x != null; x = x.next)
			if (key.equals(x.key))
				return (Value) x.val;
		return null;}

	@Override
	public boolean contains(Key key) {return (get(key) != null);}

	@Override
	public boolean isEmpty() {return (size == 0);}

	@Override
	/**
	 *@return quantity of elements 
	 */
	public int size() {return size;}

	@Override
	public Iterator<Key> keys() {return null;}

}
