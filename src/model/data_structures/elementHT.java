package model.data_structures;

public class elementHT<Key extends Comparable<Key>, Value extends Comparable<Value>> {
	private Key Key;
	private Value element;
	public elementHT(Key llave, Value element) {this.Key = llave; this.element = element;}
	public Value getElement() {return this.element;}
	public Key key() {return this.Key;}
	public void setVal(Value val) {this.element = val;}
}
