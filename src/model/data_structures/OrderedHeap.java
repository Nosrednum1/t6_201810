package model.data_structures;

import Api.IHeapArray;


/**
 * @author Anderson Barrag�n Agudelo
 * <br><i>class in working</i>
 */
public class OrderedHeap<T extends Comparable<T>> implements IHeapArray<T>{

	private T[] data;
	private int size;

	@SuppressWarnings("unchecked")
	public OrderedHeap(int capacity) {data = (T[]) new Comparable[capacity+1];}

	/**
	 * 	Take the array and make a new array whit size + 1/10(size)
	 * @param Array array to expand
	 * @return new array, with all elements of the "Array"
	 */
	public void expand() {
		T[] newArray = (T[]) new Comparable[data.length + (int) Math.ceil((data.length + 1)/8)];
		for (int i = 1; i < data.length; i++) {
			newArray[i] = data[i];
		}
		data = newArray;
	}

	@Override
	public void insert(T element) {
		if (size == data.length - 1)
			expand();

		size++;
		data[size] = element;
		reorderUp();
	}

	private void reorderUp() {
		int hijo = size();
		while (hijo > 1){
			int padre = hijo / 2;
			if (less(hijo, padre))//sale si el padre es mayor o igual al elemento actual
				break;
			exchange(hijo,padre);
			hijo = padre;
		}  
	}
	private void reorderDown() {
		int index = 1;
		while (true){
			int hijo = index*2;
			if (hijo > size())
				break;
			//si hay dos hijos -> toma el mayor si no,
			//si son iguales toma el izquierdo
			if (hijo + 1 <= size())	hijo = findMin(hijo, hijo + 1);
			if (!less(index, hijo))
				break;
			exchange(index,hijo);
			index = hijo;
		}
	}

	private int findMin(int hijoIzquierdo, int hijoDerecho) {
		return((data[hijoIzquierdo].compareTo(data[hijoDerecho])) >= 0)? hijoIzquierdo:hijoDerecho;
	}

	@Override
	public T remove() {
		if (size != 0) {
			T min = data[1];
			data[1] = data[size];
			size--;
			reorderDown();
			return min;
		}
		return null;
	}

	@Override
	public int size(){return size;}

	public String toString() {
		String ret = "";
		for (T t : data) {
			ret += t + " ";
		}
		return ret;
	}

	public T get(T element) {
		T t = null;
		for (int i = 1; i < size && t == null; i++) {
			if(data[i].compareTo(element) == 0) t = data[i];
		}
		return t;
	}

	@Override
	public boolean less(int i, int j) {
		return (data[i]!= null && data[j] != null)? data[i].compareTo(data[j]) < 0:false;
	}

	@Override
	public void exchange(int i, int j) {
		T t = data[i];
		data[i] = data[j];
		data[j] = t;
	}	
}
