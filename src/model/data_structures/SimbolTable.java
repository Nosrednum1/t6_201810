package model.data_structures;

import java.util.Iterator;

import Api.IHashTable;

public class SimbolTable<Key extends Comparable<Key>, Value extends Comparable<Value>>
implements IHashTable<Key, Value> {

	private Key[] keys;
	private Value[] values; 
	private int size;

	@SuppressWarnings("unchecked")
	public SimbolTable(int capacity) {
		keys = (Key[]) new Comparable[capacity];
		values = (Value[]) new Comparable[capacity];
	}

	@Override
	public int hash(Key key) {return 0;}

	@Override
	public void put(Key key, Value val) {
		// Search for key. Update value if found; grow table if new. 
		if(size == keys.length)expand();
		int i = rank(key); 
		if (i < size && keys[i] != null && keys[i].compareTo(key) == 0) {
			values[i] = val; return;}
		for (int j = size; j > i; j--) {
			keys[j] = keys[j-1]; values[j]= values [j-1];}
		keys[i] = key; values[i] = val; size++;
	}
	private int rank(Key key) {
		int lo = 0, hi = size-1;
		while (lo <= hi) {
			int mid = lo + (hi - lo) / 2;
			int cmp = key.compareTo(keys[mid]);
			if (cmp < 0) hi = mid - 1;
			else if (cmp > 0) lo = mid + 1;
			else return mid; }
		return lo;
	}

	@Override
	public Value get(Key key) {
		if (isEmpty())
			return null;
		int i = rank(key); 
		if (i < size && keys[i].compareTo(key) == 0) return values[i];
		else return null;
	}

	private void expand() {
		Key[] newKeys = (Key[]) new Comparable[size + size/4];
		Value[] newValues = (Value[]) new Comparable[size + size/4];
		for (int i = 0; i < keys.length; i++) {
			newKeys[i] = keys[i]; newValues[i] = values[i];}
		keys = newKeys; values = newValues;
	}

	@Override
	public boolean contains(Key key) {return get(key)!= null;}

	@Override
	public boolean isEmpty() {return size == 0;}

	@Override
	public int size() {return size;}

	@Override
	public Iterator<Key> keys() {return null;}

}
