package dataStructuresTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.SimbolTable;

public class simbolTableTest {

	private SimbolTable<Integer, String> lnht;

	@Before
	public void setupEscenario1(){
		lnht = new SimbolTable<>(10);
	}
	public void setupEscenario2() {
		lnht.put(new Integer(1), "Uno");
		lnht.put(new Integer(2), "Dos");
		lnht.put(new Integer(3), "Tres");
	}
	public void setupEscenario3() {
		lnht.put(new Integer(4), "Uno");
		lnht.put(new Integer(5), "Dos");
		lnht.put(new Integer(6), "Tres");
		lnht.put(new Integer(7), "Uno");
		lnht.put(new Integer(8), "Dos");
		lnht.put(new Integer(9), "Tres");
		lnht.put(new Integer(10), "Uno");
		lnht.put(new Integer(11), "Dos");
		lnht.put(new Integer(12), "Doce1");
	}

	@Test
	public void testPut(){
		assertTrue(lnht.isEmpty(), "la lista deber�a estar vac�a");
		setupEscenario2();
		assertEquals("No carg� todos los elementos", 3, lnht.size());
		setupEscenario3();
		assertEquals("No carg� todos los elementos", 12, lnht.size());
	}
	@Test
	public void testGet() {
		assertNull("No deber�a existir nada", lnht.get(new Integer(12)));
		assertTrue(!lnht.contains(new Integer(12)), "No deber�a contener nada");
		setupEscenario3();
		assertEquals("Nos est� cargando bien", "Doce1", lnht.get(new Integer(12)));
		lnht.put(new Integer(12), "Doce2");
		assertEquals("No sobreescribe los elementos", "Doce2", lnht.get(new Integer(12)));
		lnht.put(new Integer(12), "Doce3");
		assertEquals("No sobreescribe los elementos", "Doce3", lnht.get(new Integer(12)));
	}

}
