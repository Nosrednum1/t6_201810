package dataStructuresTests;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.SeparateChainingHashTable;

public class separateChainingHTTest {

	private SeparateChainingHashTable<Integer, String> scht;

	@Before
	public void setupEscenario1(){
		scht = new SeparateChainingHashTable<>(10);
	}
	public void setupEscenario2() {

	}
	public void setupEscenario3() {

	}

	@Test
	public void testAdd(){
		assertTrue(scht.isEmpty(), "la lista deber�a estar vac�a");
		setupEscenario2();
		assertEquals("No carg� todos los elementos", 3, scht.size());
		setupEscenario3();
		assertEquals("No carg� todos los elementos", 5, scht.size());
	}

}
